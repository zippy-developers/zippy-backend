module.exports = {
  testMatch: ['<rootDir>/tests/**/*.test.js'],

  // Jest reporters, including "jest-junit" for JUnit XML reports
  reporters: ['default', ['jest-junit', { outputDirectory: 'results' }]],
};
