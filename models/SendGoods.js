import mongoose from 'mongoose';
const { Schema } = mongoose;

const sendGoodsSchema = new Schema(
  {
    userID: {
      type: String,
      required: true,
    },
    senderName: {
      type: String,
      required: true,
    },
    senderEmail: {
      type: String,
      required: true,
    },
    senderPhone: {
      type: String,
      required: true,
    },
    receiverName: {
      type: String,
      required: true,
    },
    receiverAddress: {
      type: String,
      required: true,
    },
    receiverState: {
      type: String,
      required: true,
    },
    receiverPostalCode: {
      type: String,
      required: true,
    },
    receiverCountry: {
      type: String,
      required: true,
    },
    receiverEmail: {
      type: String,
      required: true,
    },
    receiverPhone: {
      type: String,
      required: true,
    },
    orderSize: {
      type: String,
      required: true,
    },
    orderWeight: {
      type: String,
      required: true,
    },
    orderCategory: {
      type: String,
      required: true,
    },
    orderImage: {
      type: String,
      required: true,
    },
    orderReceiveDate: {
      type: String,
      required: true,
    },
    orderPlaceDate: {
      type: String,
      required: true,
    },
    orderCost: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const SendGoodsModel = mongoose.model(
  'SendGoods',
  sendGoodsSchema
);

export default SendGoodsModel;
