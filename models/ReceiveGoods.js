import mongoose from 'mongoose';
const { Schema } = mongoose;

const receiveGoodsSchema = new Schema(
  {
    userID: {
      type: String,
      required: true,
    },
    senderName: {
      type: String,
      required: true,
    },
    senderAddress: {
      type: String,
      required: true,
    },
    senderState: {
      type: String,
      required: true,
    },
    senderPostalCode: {
      type: String,
      required: true,
    },
    senderCountry: {
      type: String,
      required: true,
    },
    senderEmail: {
      type: String,
      required: true,
    },
    senderPhone: {
      type: String,
      required: true,
    },
    receiverName: {
      type: String,
      required: true,
    },
    receiverEmail: {
      type: String,
      required: true,
    },
    receiverPhone: {
      type: String,
      required: true,
    },
    orderSize: {
      type: String,
      required: true,
    },
    orderWeight: {
      type: String,
      required: true,
    },
    orderCategory: {
      type: String,
      required: true,
    },
    orderImage: {
      type: String,
      required: true,
    },
    orderReceiveDate: {
      type: String,
      required: true,
    },
    orderPlaceDate: {
      type: String,
      required: true,
    },
    orderCost: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const ReceiveGoodsModel = mongoose.model('ReceiveGoods', receiveGoodsSchema);

export default ReceiveGoodsModel;
