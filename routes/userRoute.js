import express from 'express';
import {
  updateProfileImage,
  getUserById,
  updatePassword,
  updateUserDetails,
} from '../controllers/userController.js';

const router = express.Router();

router.put('/avatar/:userId', updateProfileImage);
router.get('/:userId', getUserById);
router.put('/:userId', updateUserDetails);
router.put('/password/:userId', updatePassword);

export default router;
