import express from 'express';
import { initiatePasswordReset } from '../controllers/resetPasswordController.js';

const router = express.Router();

router.post('/:id/:token', initiatePasswordReset);

export default router;
