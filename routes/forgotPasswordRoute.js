import express from 'express';
import { initiatePasswordForgot } from '../controllers/forgotPasswordController.js';

const router = express.Router();

router.post('/', initiatePasswordForgot);

export default router;
