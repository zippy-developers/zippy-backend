import express from 'express';
import {
  login,
  logout,
  register,
  checkEmailAvailability,
  checkUsernameAvailability,
} from '../controllers/authController.js';

const router = express.Router();

router.post('/register', register);
router.post('/login', login);
router.post('/logout', logout);

router.get('/email/:email', checkEmailAvailability);
router.get('/username/:username', checkUsernameAvailability);

export default router;
