import express from 'express';
import {
  sendGoodsRequest,
  getAllSendGoodsRequests,
  getSendGoodsRequestsByUser,
  deleteSendGoodsRequest,
} from '../controllers/sendGoodsController.js';

const router = express.Router();

router.post('/', sendGoodsRequest);
router.get('/', getAllSendGoodsRequests);
router.post('/view/:userID', getSendGoodsRequestsByUser);
router.delete('/:sendGoodsID', deleteSendGoodsRequest);

export default router;
