import express from 'express';
import {
  receiveGoodsRequest,
  getAllReceiveGoodsRequests,
  getReceiveGoodsByUser,
  deleteReceiveGoodsRequest,
} from '../controllers/receiveGoodsController.js';

const router = express.Router();

router.post('/', receiveGoodsRequest);
router.get('/', getAllReceiveGoodsRequests);
router.post('/view/:userID', getReceiveGoodsByUser);
router.delete('/:receiveGoodsID', deleteReceiveGoodsRequest);

export default router;
