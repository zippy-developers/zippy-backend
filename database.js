import mongoose from 'mongoose';
import dotenv from 'dotenv';

export const connectDB = async () => {
  const dbUrl = process.env.MONGO;
  try {
    await mongoose.connect(dbUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('Connected to MongoDB server!');
  } catch (error) {
    console.error('MongoDB connection error:', error.message);
    process.exit(1);
  }
};

export const closeDB = async () => {
  await mongoose.connection.close();
};
