import UserModel from '../models/User.js';
import { createError } from '../utils/error.js';
import bcrypt from 'bcryptjs';
import Jwt from 'jsonwebtoken';

//Profile image update
export const updateProfileImage = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const { avatar } = req.body;
    const user = await UserModel.findByIdAndUpdate(userId, { avatar });
    if (!user) throw createError(404, 'User not found');
    res.status(200).json(user);
  } catch (error) {
    next(error);
  }
};

//Get user by id
export const getUserById = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const user = await UserModel.findById(userId);
    if (!user) throw createError(404, 'User not found');
    const accessToken = Jwt.sign(
      { id: user._id, role: user.role },
      process.env.JWT_SECRET
    );
    const { password, role, ...otherDetails } = user._doc;
    res
      .cookie('accessToken', accessToken, { httpOnly: true })
      .status(200)
      .json({ details: { ...otherDetails }, role });
  } catch (error) {
    next(error);
  }
};

//update password
export const updatePassword = async (req, res, next) => {
  try {
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(req.body.password, salt);
    const { userId } = req.params;
    const user = await UserModel.findByIdAndUpdate(userId, {
      password: hashedPassword,
    });
    if (!user) throw createError(404, 'User not found');
    res.status(200).json(user);
  } catch (error) {
    next(error);
  }
};

//update user details
export const updateUserDetails = async (req, res, next) => {
  try {
    const { userId } = req.params;
    const { formData } = req.body;
    const user = await UserModel.findByIdAndUpdate(userId, {
      firstName: formData.firstName,
      lastName: formData.lastName,
      email: formData.email,
      phone: formData.phone,
      address: formData.address,
    });
    if (!user) throw createError(404, 'User not found');
    res.status(200).json(user);
  } catch (error) {
    next(error);
  }
};
