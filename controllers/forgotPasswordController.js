import jwt from 'jsonwebtoken';
import nodemailer from 'nodemailer';
import User from '../models/User.js';

import dotenv from 'dotenv';
dotenv.config();

if (process.env.NODE_ENV !== 'production') {
  const url = 'https://zippy.tharindu.me';
} else {
  const url = 'http://localhost:5173';
}

export const initiatePasswordForgot = async (req, res) => {
  const { email } = req.body;

  const emailSender = process.env.EMAIL_SENDER;
  const emailSenderPassword = process.env.EMAIL_SENDER_PASSWORD;

  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: emailSender,
      pass: emailSenderPassword,
    },
    secure: true,
  });

  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(404).json({ error: 'User not found.' });
    }

    const resetToken = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, {
      expiresIn: '1h',
    });

    const mailOptions = {
      from: emailSender,
      to: email,
      subject: 'Password Reset Instructions',
      text: `Click on the following link to reset your password: ${url}/auth/reset-password/${user._id}/${resetToken}`,
    };

    await transporter.sendMail(mailOptions);
    return res
      .status(200)
      .json({ message: 'Password reset instructions sent to your email.' });
  } catch (error) {
    console.error('Error initiating password reset: ', error);
    return res.status(500).json({ error: 'Internal server error.' });
  }
};
