import SendGoodsModel from '../models/SendGoods.js';

// Delivery Request Controller
export const sendGoodsRequest = async (req, res, next) => {
  try {
    const newSendGoodsRequest = await SendGoodsModel.create(req.body);
    res.status(201).json(newSendGoodsRequest);
  } catch (err) {
    console.log(err);
    next(err);
  }
};

// Get all send goods
export const getAllSendGoodsRequests = async (req, res, next) => {
  try {
    const sendGoodsRequests = await SendGoodsModel.find();
    res.status(200).json(sendGoodsRequests);
  } catch (err) {
    console.log(err);
    next(err);
  }
};

// Get all send goods by user
export const getSendGoodsRequestsByUser = async (req, res, next) => {
  try {
    const sendGoodsRequests = await SendGoodsModel.find({
      userID: req.params.userID,
    });
    res.status(200).json(sendGoodsRequests);
  } catch (err) {
    console.log(err);
    next(err);
  }
};

export const deleteSendGoodsRequest = async (req, res, next) => {
  try {
    const sendGoodsRequest = await SendGoodsModel.findByIdAndDelete(
      req.params.sendGoodsID
    );
    res.status(200).json(sendGoodsRequest);
  } catch (err) {
    console.log(err);
    next(err);
  }
};
