import UserModel from '../models/User.js';
import bcrypt from 'bcryptjs';
import { createError } from '../utils/error.js';
import Jwt from 'jsonwebtoken';

// Register Controller
export const register = async (req, res, next) => {
  try {
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(req.body.password, salt);

    const newUser = await UserModel({
      ...req.body,
      password: hashedPassword,
    });
    await newUser.save();
    res.status(201).send(newUser);
  } catch (err) {
    if (
      err.code === 11000 &&
      err.message.includes(`username: "${req.body.username}"`)
    )
      res.status(400).json({ message: 'Username already exists!' });
    // check if email duplicate
    else if (
      err.code === 11000 &&
      err.message.includes(`email: "${req.body.email}"`)
    )
      res.status(400).json({ message: 'Email already exists!' });
    else next(err);
  }
};

// Check email is already registered
export const checkEmailAvailability = async (req, res) => {
  const { email } = req.params;

  try {
    const existingUser = await UserModel.findOne({ email });
    res.json({ available: !existingUser });
  } catch (error) {
    console.error(error);
    res
      .status(500)
      .json({ message: 'An error occurred while checking email availability' });
  }
};

//check username is already registered
export const checkUsernameAvailability = async (req, res) => {
  const { username } = req.params;

  try {
    const existingUser = await UserModel.findOne({ username });
    res.json({ available: !existingUser });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: 'An error occurred while checking username availability',
    });
  }
};

// User Login Controller
export const login = async (req, res, next) => {
  try {
    const user = await UserModel.findOne({ username: req.body.username });
    if (!user) return next(createError(400, 'User not found!'));
    const isPasswordMatch = await bcrypt.compare(
      req.body.password,
      user.password
    );
    if (!isPasswordMatch) return next(createError(400, 'Invalid credentials!'));
    const accessToken = Jwt.sign(
      { id: user._id, role: user.role },
      process.env.JWT_SECRET
    );
    const { password, role, ...otherDetails } = user._doc;
    res
      .cookie('accessToken', accessToken, { httpOnly: true })
      .status(200)
      .json({ details: { ...otherDetails }, role });
  } catch (err) {
    next(err);
  }
};

// User Logout Controller
export const logout = async (req, res, next) => {
  try {
    if (req.cookies.accessToken) {
      verifyToken(req, res, () => {
        const userID = req.user.id;
        const userRole = req.user.role;
        res
          .clearCookie('accessToken')
          .status(200)
          .send(`${userRole}: (${userID}) logged out successfully!`);
      });
    } else {
      res.status(401).send('User already logged out!');
    }
  } catch (err) {
    next(err);
  }
};
