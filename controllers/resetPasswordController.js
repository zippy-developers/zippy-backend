import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import User from '../models/User.js';

export const initiatePasswordReset = async (req, res) => {
  const { id, token } = req.params;
  const { password } = req.body;

  jwt.verify(token, process.env.JWT_SECRET, async (err, decodedToken) => {
    if (err) {
      return res.status(400).json({ error: 'Invalid token.' });
    } else {
      bcrypt
        .hash(password, 10)
        .then((hashedPassword) => {
          User.findByIdAndUpdate({ _id: id }, { password: hashedPassword })
            .then(() => {
              return res.status(200).json({ message: 'Password updated.' });
            })
            .catch((error) => {
              return res
                .status(500)
                .json({ error: 'Internal server error.', error });
            });
        })
        .catch((error) => {
          return res
            .status(500)
            .json({ error: 'Internal server error.', error });
        });
    }
  });
};
