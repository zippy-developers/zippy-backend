import ReceiveGoodsModel from '../models/ReceiveGoods.js';

// Delivery Request Controller
export const receiveGoodsRequest = async (req, res, next) => {
  try {
    const newReceiveGoodsRequest = await ReceiveGoodsModel.create(req.body);
    res.status(201).json(newReceiveGoodsRequest);
  } catch (err) {
    console.log(err);
    next(err);
  }
};

// Get all receive goods
export const getAllReceiveGoodsRequests = async (req, res, next) => {
  try {
    const receiveGoodsRequests = await ReceiveGoodsModel.find();
    res.status(200).json(receiveGoodsRequests);
  } catch (err) {
    console.log(err);
    next(err);
  }
};

// Get all delivery requests by user
export const getReceiveGoodsByUser = async (req, res, next) => {
  try {
    const receiveGoodsRequests = await ReceiveGoodsModel.find({
      userID: req.params.userID,
    });
    res.status(200).json(receiveGoodsRequests);
  } catch (err) {
    console.log(err);
    next(err);
  }
};

export const deleteReceiveGoodsRequest = async (req, res, next) => {
  try {
    const receiveGoodsRequest = await ReceiveGoodsModel.findByIdAndDelete(
      req.params.receiveGoodsID
    );
    res.status(200).json(receiveGoodsRequest);
  } catch (err) {
    console.log(err);
    next(err);
  }
};
