import ReceiveGoodsModel from '../models/ReceiveGoods';
import request from 'supertest';
import app from '../server';
import { connectDB, closeDB } from '../database';

describe('Receive Goods Model', () => {
  beforeAll(async () => {
    await connectDB(); // Connect to the test database
  });

  afterAll(async () => {
    await ReceiveGoodsModel.deleteMany({ status: 'testing' });
    await closeDB();
  });

  it('Should save a receive goods request to the database', async () => {
    const receiveGoodsData = {
      userID: '6515af3d9cf182950301ae92',
      receiverName: 'John Doe',
      receiverEmail: 'johndoe@example.com',
      receiverPhone: '+1 (123) 456-7890',
      senderName: 'Jane Smith',
      senderAddress: '456 Elm Street',
      senderState: 'New York',
      senderPostalCode: '10001',
      senderCountry: 'USA',
      senderEmail: 'janesmith@example.com',
      senderPhone: '+1 (987) 654-3210',
      orderSize: '2',
      orderWeight: '5',
      orderImage: 'https://example.com/order-image.jpg',
      orderReceiveDate: '2023-09-15',
      orderPlaceDate: '2023-12-14',
      orderCost: '500',
      orderCategory: 'Electronics',
      status: 'testing',
    };

    // Make a POST request to create
    const response = await request(app)
      .post('/api/receive-goods')
      .send(receiveGoodsData);

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('_id');
    expect(response.body.userID).toBe(receiveGoodsData.userID);
    expect(response.body.receiverName).toBe(receiveGoodsData.receiverName);
    expect(response.body.receiverEmail).toBe(receiveGoodsData.receiverEmail);
    expect(response.body.receiverPhone).toBe(receiveGoodsData.receiverPhone);
    expect(response.body.senderName).toBe(receiveGoodsData.senderName);
    expect(response.body.senderAddress).toBe(receiveGoodsData.senderAddress);
    expect(response.body.senderState).toBe(receiveGoodsData.senderState);
    expect(response.body.senderPostalCode).toBe(
      receiveGoodsData.senderPostalCode
    );
    expect(response.body.senderCountry).toBe(receiveGoodsData.senderCountry);
    expect(response.body.senderEmail).toBe(receiveGoodsData.senderEmail);
    expect(response.body.senderPhone).toBe(receiveGoodsData.senderPhone);
    expect(response.body.orderSize).toBe(receiveGoodsData.orderSize);
    expect(response.body.orderWeight).toBe(receiveGoodsData.orderWeight);
    expect(response.body.orderImage).toBe(receiveGoodsData.orderImage);
    expect(response.body.orderReceiveDate).toBe(
      receiveGoodsData.orderReceiveDate
    );
    expect(response.body.orderPlaceDate).toBe(receiveGoodsData.orderPlaceDate);
    expect(response.body.orderCost).toBe(receiveGoodsData.orderCost);
    expect(response.body.orderCategory).toBe(receiveGoodsData.orderCategory);
    expect(response.body.status).toBe(receiveGoodsData.status);
  });
});
