import SendGoodsModel from '../models/SendGoods';
import request from 'supertest';
import app from '../server';
import { connectDB, closeDB } from '../database';

describe('Send Goods Model', () => {
  beforeAll(async () => {
    await connectDB();
  });

  afterAll(async () => {
    await SendGoodsModel.deleteMany({ status: 'testing' });
    await closeDB();
  });

  it('Should save a send goods request to the database', async () => {
    const sendGoodsData = {
      userID: '6515af3d9cf182950301ae92',
      senderName: 'John Doe',
      senderEmail: 'johndoe@example.com',
      senderPhone: '+1 (123) 456-7890',
      receiverName: 'Jane Smith',
      receiverAddress: '456 Elm Street',
      receiverState: 'New York',
      receiverPostalCode: '10001',
      receiverCountry: 'USA',
      receiverEmail: 'janesmith@example.com',
      receiverPhone: '+1 (987) 654-3210',
      orderSize: '2',
      orderWeight: '5',
      orderImage: 'https://example.com/order-image.jpg',
      orderReceiveDate: '2023-09-15',
      orderPlaceDate: '2023-12-14',
      orderCost: '500',
      orderCategory: 'Electronics',
      status: 'testing',
    };

    // Make a POST request to create the delivery request
    const response = await request(app)
      .post('/api/send-goods')
      .send(sendGoodsData);

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('_id');
    expect(response.body.userID).toBe(sendGoodsData.userID);
    expect(response.body.senderName).toBe(sendGoodsData.senderName);
    expect(response.body.senderEmail).toBe(sendGoodsData.senderEmail);
    expect(response.body.senderPhone).toBe(sendGoodsData.senderPhone);
    expect(response.body.receiverName).toBe(sendGoodsData.receiverName);
    expect(response.body.receiverAddress).toBe(sendGoodsData.receiverAddress);
    expect(response.body.receiverState).toBe(sendGoodsData.receiverState);
    expect(response.body.receiverPostalCode).toBe(
      sendGoodsData.receiverPostalCode
    );
    expect(response.body.receiverCountry).toBe(sendGoodsData.receiverCountry);
    expect(response.body.receiverEmail).toBe(sendGoodsData.receiverEmail);
    expect(response.body.receiverPhone).toBe(sendGoodsData.receiverPhone);
    expect(response.body.orderSize).toBe(sendGoodsData.orderSize);
    expect(response.body.orderWeight).toBe(sendGoodsData.orderWeight);
    expect(response.body.orderImage).toBe(sendGoodsData.orderImage);
    expect(response.body.orderReceiveDate).toBe(sendGoodsData.orderReceiveDate);
    expect(response.body.orderPlaceDate).toBe(sendGoodsData.orderPlaceDate);
    expect(response.body.orderCost).toBe(sendGoodsData.orderCost);
    expect(response.body.orderCategory).toBe(sendGoodsData.orderCategory);
    expect(response.body.status).toBe(sendGoodsData.status);
  });
});
