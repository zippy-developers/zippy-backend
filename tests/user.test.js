import UserModel from '../models/User'; // Adjust the path as needed
import request from 'supertest';
import app from '../server';
import { connectDB, closeDB } from '../database';

describe('User Model', () => {
  beforeAll(async () => {
    await connectDB(); // Connect to the test database
  });

  afterAll(async () => {
    await UserModel.deleteMany({ role: 'testuser' });
    await closeDB();
  });

  it('Should save a user to the database', async () => {
    const userData = {
      firstName: 'John',
      lastName: 'Doe',
      username: 'johndoe',
      email: 'john@example.com',
      phone: '1234567890',
      password: 'password123',
      role: 'testuser',
      address: '123 Main St',
      isActive: true,
    };

    // Make a POST request to create the user
    const response = await request(app)
      .post('/api/auth/register')
      .send(userData);

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('_id');
    expect(response.body.firstName).toBe(userData.firstName);
    expect(response.body.lastName).toBe(userData.lastName);
    expect(response.body.username).toBe(userData.username);
    expect(response.body.email).toBe(userData.email);
    expect(response.body.phone).toBe(userData.phone);
    expect(response.body.role).toBe(userData.role);
    expect(response.body.address).toBe(userData.address);
    expect(response.body.isActive).toBe(userData.isActive);
  });

  it('Should login a user', async () => {
    // Make a POST request to login the user
    const response = await request(app).post('/api/auth/login').send({
      username: 'johndoe',
      password: 'password123',
    });
    expect(response.status).toBe(200);
  });
});
