import express from 'express';
import dotenv from 'dotenv';
import authRoute from './routes/authRoute.js';
import sendGoodsRoute from './routes/sendGoodsRoute.js';
import receiveGoodsRoute from './routes/receiveGoodsRoute.js';
import forgotPasswordRoute from './routes/forgotPasswordRoute.js';
import resetPasswordRoute from './routes/resetPasswordRoute.js';
import userRoute from './routes/userRoute.js';
import cookieParser from 'cookie-parser';
import cors from 'cors';

const isProduction = process.env.NODE_ENV === 'production';

const app = express();

dotenv.config();

const corsOrigin = isProduction
  ? process.env.CORS_ORIGIN_PROD
  : process.env.CORS_ORIGIN_DEV;

app.use(
  cors({
    origin: corsOrigin,
    credentials: true,
  })
);

app.use(cookieParser());
app.use(express.json());

// Routes setup
app.use('/api/auth', authRoute);
app.use('/api/auth/forgot-password', forgotPasswordRoute);
app.use('/api/auth/reset-password', resetPasswordRoute);
app.use('/api/send-goods', sendGoodsRoute);
app.use('/api/receive-goods', receiveGoodsRoute);
app.use('/api/user', userRoute);

export default app;
